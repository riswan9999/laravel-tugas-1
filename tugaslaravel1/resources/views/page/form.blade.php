@extends('layout.master')
@section('title')
	Buat Acccount baru
@endsection
@section('content')	
	<p><b>Sign Up Form</b></p>
	<form method="post" action="/welcome">
        @csrf
		<p>First name :</p>
		<p><input type="text" name="firstname"></p>
		<p>Last name :</p>
		<p><input type="text" name="lastname"></p>
		<p>Gender:</p>
		<p>
		   <input type="radio" name="gender">Male<br>
		   <input type="radio" name="gender">Female
		</p>
		<p>Nationality :</p>
		<p><select name="nation">
			 <option value="Indonesia">Indonesia</option> 
			 <option value="Amerika">Amerika</option> 
			 <option value="Inggris">Inggris</option> 
		   </select>
		</p>
		<p>Language Spoken</p>
		<p>
		   <input type="checkbox">Bahasa Indonesia<br>
		   <input type="checkbox">English<br>
		   <input type="checkbox">Other<br>
		</p>
		<p>Bio</p>
		<p><textarea rows="6" cols="20"></textarea></p>
		<p><input type="submit" value="Sign Up"></p>
	</form>
@endsection